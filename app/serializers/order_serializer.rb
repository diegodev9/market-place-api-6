# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id         :bigint           not null, primary key
#  total      :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class OrderSerializer
  include FastJsonapi::ObjectSerializer
  belongs_to :user
  has_many :products

  cache_options store: Rails.cache, namespace: 'jsonapi-serializer', expires_in: 12.hours
end
