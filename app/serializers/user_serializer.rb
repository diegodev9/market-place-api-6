# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  email           :string           not null
#  password_digest :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
# serializer
class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :email
  has_many :products

  cache_options store: Rails.cache, namespace: 'jsonapi-serializer', expires_in: 12.hours
end
