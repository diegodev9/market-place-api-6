# frozen_string_literal: true

module Api
  module V1
    # Users Controller
    class UsersController < ApplicationController
      before_action :set_user, only: %i[show update destroy check_owner]
      before_action :check_owner, only: %i[update destroy]

      # GET /users/1
      def show
        option = { include: [:products] }
        render json: UserSerializer.new(@user, option).serializable_hash
      end

      # POST /users
      def create
        @user = User.new(user_params)

        if @user.save
          render json: UserSerializer.new(@user).serializable_hash, status: :created
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /users/1
      def update
        if @user.update(user_params)
          render json: UserSerializer.new(@user).serializable_hash, status: :ok
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # DELETE /users/1
      def destroy
        @user.destroy
        head :no_content
      end

      private

      def set_user
        @user = User.find(params[:id])
      end

      def check_owner
        head :forbidden unless @user.id == current_user&.id
      end

      def user_params
        params.require(:user).permit(:email, :password)
      end
    end
  end
end
