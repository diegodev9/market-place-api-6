# frozen_string_literal: true

module Api
  module V1
    # Product controller
    class ProductsController < ApplicationController
      before_action :set_product, only: %i[show update destroy check_owner]
      before_action :check_owner, only: %i[update destroy]
      before_action :check_login, only: %i[create]

      include Pagy::Backend
      include Paginable

      # GET /products
      def index
        @products = Product.includes(:user).search(params)
        @pagy, @products = pagy(@products, page: current_page, items: 5)

        options = get_links_serializer_options('api_v1_products_path', @pagy)
        options[:include] = [:user]

        render json: ProductSerializer.new(@products, options).serializable_hash
      end

      # GET /products/1
      def show
        options = { include: [:user] }
        render json: ProductSerializer.new(@product, options).serializable_hash
      end

      def create
        product = Product.new(product_params)
        product.user_id = @current_user.id

        if product.save
          render json: ProductSerializer.new(product).serializable_hash, status: :created
        else
          render json: { errors: product.errors }, status: :unprocessable_entity
        end
      end

      def update
        if @product.update(product_params)
          render json: ProductSerializer.new(@product).serializable_hash
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @product.destroy
        head :no_content
      end

      private

      def check_owner
        head :forbidden unless @product.user_id == current_user&.id
      end

      def set_product
        @product = Product.find(params[:id])
      end

      def product_params
        params.require(:product).permit(:price, :published, :title, :user_id)
      end
    end
  end
end
