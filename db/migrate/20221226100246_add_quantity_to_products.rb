# frozen_string_literal: true

# add quantity to products
class AddQuantityToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :quantity, :integer, default: 0
  end
end
