# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderMailer, type: :mailer do
  let(:new_product) { create(:product) }
  let(:new_order) { create(:order, user_id: new_product.user_id) }
  let(:new_placement) { create(:placement, order_id: new_order.id, product_id: new_product.id) }
  let(:mail) { described_class.send_confirmation(new_order) }

  describe 'send mail to the user from the order passed in' do
    context 'when a new order generated' do
      it { expect(mail.subject).to eq('Order Confirmation') }
      it { expect(mail.to.to_s).to match(new_order.user.email) }
      it { expect(mail.from.to_s).to match('no-reply@marketplace.com') }
      it { expect(mail.body.encoded).to match("Order: ##{new_order.id}") }
      it { expect(mail.body.encoded).to match("You ordered #{new_order.products.count}") }
    end
  end
end
