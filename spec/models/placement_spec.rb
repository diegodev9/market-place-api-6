# frozen_string_literal: true

# == Schema Information
#
# Table name: placements
#
#  id         :bigint           not null, primary key
#  quantity   :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  order_id   :bigint           not null
#  product_id :bigint           not null
#
# Indexes
#
#  index_placements_on_order_id    (order_id)
#  index_placements_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_id => orders.id)
#  fk_rails_...  (product_id => products.id)
#
require 'rails_helper'

RSpec.describe Placement, type: :model do
  describe 'creates a placement' do
    context 'when user creates an order' do
      let(:new_product) { create(:product) }
      let(:new_order) { build(:order, product_ids: [new_product.id]) }
      # let(:new_placement) { create(:placement, order_id: new_order.id, product_id: new_product.id, quantity: 1) }

      it 'creates a placement' do
        expect { new_order.save }.to change(described_class, :count).by(1)
      end
    end
  end
end
