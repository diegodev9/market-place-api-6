# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  email           :string           not null
#  password_digest :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
require 'rails_helper'

RSpec.describe User, type: :model do
  let(:new_user) { create(:user) }

  describe 'associations' do
    it { is_expected.to have_many(:products) }
  end

  describe 'validations' do
    subject { new_user }

    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to allow_value('example@email.com').for(:email) }
    it { is_expected.to validate_presence_of(:password_digest) }
  end

  context 'when email is valid' do
    it 'is valid' do
      expect(new_user).to be_valid
    end
  end

  context 'when email is invalid' do
    it 'is not invalid' do
      new_user2 = described_class.new(email: 'invalid', password: Faker::Internet.password)
      expect(new_user2).not_to be_valid
    end

    it 'if email is taken should not be valid' do
      other_user_email = new_user.email
      new_user2 = described_class.new(email: other_user_email, password: Faker::Internet.password)

      expect(new_user2).not_to be_valid
    end
  end

  context 'when create user' do
    it 'increment user count' do
      expect do
        new_user
      end.to change(described_class, :count).by(1)
    end
  end

  context 'when destroy user' do
    it 'decrement user count on user delete' do
      new_user_account = new_user
      expect do
        new_user_account.destroy
      end.to change(described_class, :count).by(-1)
    end

    it 'destroy linked products' do
      create(:product, user_id: new_user.id)
      expect do
        new_user.destroy
      end.to change(Product, :count).by(-1)
    end
  end
end
