# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id         :bigint           not null, primary key
#  price      :decimal(, )
#  published  :boolean
#  quantity   :integer          default(0)
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_products_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:new_product) { create(:product) }

  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:price) }
    it { is_expected.to validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  end

  context 'when create a product' do
    it 'creates' do
      expect do
        new_product
      end.to change(described_class, :count).by(1)
    end

    it 'has a positive price' do
      expect do
        create(:product, price: -1)
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'when destroy a product' do
    it 'destroys' do
      new_product
      expect do
        new_product.destroy
      end.to change(described_class, :count).by(-1)
    end
  end

  context 'when using filters' do
    let(:new_product) { create_list(:product, 2, title: 'Tostadora') }
    let(:another_products) { create_list(:product, 5) }

    before do
      new_product
      another_products
    end

    it 'filters by name' do
      expect(described_class.filter_by_title('Tostadora').count).to eq(2)
    end

    it 'filters by name and sort them' do
      expect(described_class.filter_by_title('Tostadora')).to match(described_class.filter_by_title('Tostadora').sort)
    end

    it 'filters products by price and sort them', skip_before: true do
      another_products
      expect(described_class.above_or_equal_to_price(200)).to match(described_class.above_or_equal_to_price(200).sort)
    end

    it 'filters products by price lower and sort them', skip_before: true do
      another_products
      expect(described_class.lower_or_equal_to_price(200)).to match(described_class.lower_or_equal_to_price(200).sort)
    end

    it 'sort product by most recent', skip_before: true do
      another_products
      expect(described_class.recent.to_a).to match(described_class.order(created_at: :asc))
    end

    it 'does not find "videogame" and "100" as min price' do
      search_hash = { keyword: 'videogame', min_price: 100 }
      expect(described_class.search(search_hash)).to match([])
    end

    it 'does find cheap product' do
      search_hash = { keyword: 'Tostadora', min_price: 50, max_price: 150 }
      expect(described_class.search(search_hash)).not_to be_nil
    end

    it 'gets all products with no parameters' do
      expect(described_class.search({}).to_a).to match(described_class.all)
    end

    it 'search by product ids' do
      product = new_product
      search_hash = { product_ids: [product.first.id] }
      expect(described_class.search(search_hash)).to match(described_class.where(id: product.first.id))
    end
  end
end
