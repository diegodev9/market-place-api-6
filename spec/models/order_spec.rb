# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id         :bigint           not null, primary key
#  total      :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Order, type: :model do

  context 'when create a new order' do
    let(:new_order) { create(:order) }
    let(:new_product) { create(:product, quantity: 1) }

    # it { is_expected.to validate_presence_of(:total) }
    # it { is_expected.to validate_numericality_of(:total).is_greater_than_or_equal_to(0) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to have_many(:products) }
    it { is_expected.to have_many(:placements) }

    it 'does not create a new order if product quantity is unavailable' do
      new_order.placements << Placement.new(product_id: new_product.id, quantity: 2)
      expect(new_order).not_to be_valid
    end
  end

  context 'when sets several products' do
    let(:new_user) { create(:user) }
    let(:new_products) { create_list(:product, 5) }
    let(:products_ids) { Product.last(5).map(&:id) }
    let(:new_order) { create(:order, user_id: new_user.id, product_ids: products_ids) }

    it 'sets total' do
      expect(Product.last(5).map(&:price).sum).to eq(new_order.total)
    end
  end

  context 'when create 1 order with product quantity' do
    let(:product1) { create(:product, quantity: 2) }
    let(:product2) { create(:product, quantity: 3) }
    let(:products_ids) { Product.last(2).map(&:id) }
    let(:new_order) { build(:order, product_ids: products_ids) }

    it 'builds 2 placements for the order' do
      new_order.build_placements_with_product_ids_and_quantities [
        { product_id: product1.id, quantity: 2 }, { product_id: product2.id, quantity: 3 }
      ]

      expect { new_order.save }.to change(Placement, :count).by(2)
    end
  end
end
