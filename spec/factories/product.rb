# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id         :bigint           not null, primary key
#  price      :decimal(, )
#  published  :boolean
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_products_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :product do
    title { Faker::Commerce.product_name }
    price { Faker::Number.decimal(l_digits: 4, r_digits: 2) }
    published { %w[true false].sample }
    quantity { rand(0..10) }
    user
  end
end
