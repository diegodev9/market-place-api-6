# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'api/v1/placements', type: :request do
  context 'when creates an order with product quantity' do
    let(:new_user) { create(:user) }
    let(:new_product) { create(:product) }
    let(:authorization) { JsonWebToken.encode(user_id: new_user.id) }
    let(:product_quantity) { new_product.quantity }

    it 'decreases the product quantity by the placement quantity' do
      post api_v1_orders_url,
           params: { order: { product_ids_and_quantities: [{ product_id: new_product.id, quantity: 1 }] } }, as: :json,
           headers: { Authorization: authorization }
      product_quantity = new_product.quantity
      expect(Product.find(new_product.id).quantity).to eq(product_quantity - 1)
    end
  end
end
