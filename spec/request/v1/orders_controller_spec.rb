# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'api/v1/order', type: :request do
  let(:new_product) { create(:product) }
  let(:new_order) { create(:order, user_id: new_product.user_id) }
  let(:new_placement) { create(:placement, product_id: new_product.id, order_id: new_order.id) }
  let(:authorization) { JsonWebToken.encode(user_id: new_order.user.id) }
  let(:json_response) { JSON.parse(response.body, symbolize_names: true) }

  describe 'user ask for orders' do
    context 'when user is not logged in' do
      before do
        get api_v1_orders_url, as: :json
      end

      it 'forbids orders' do
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is logged in' do
      before do
        get api_v1_orders_url, headers: { Authorization: authorization }, as: :json
      end

      it 'show orders' do
        expect(response).to have_http_status(:success)
      end

      it 'show orders response contains data' do
        expect(json_response[:data].count).to eq(new_order.user.orders.count)
      end

      # using pagination
      it { expect(new_order.user.orders.count).to eq(json_response[:data].count) }
      it { expect(json_response.dig(:links, :first)).not_to be_nil }
      it { expect(json_response.dig(:links, :last)).not_to be_nil }
      it { expect(json_response.dig(:links, :prev)).not_to be_nil }
      it { expect(json_response.dig(:links, :next)).not_to be_nil }
    end
  end

  describe 'if user ask for one order' do
    context 'when user is not logged in' do
      before do
        get api_v1_order_url(new_order.id), as: :json
      end

      it 'forbids order' do
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is logged in' do
      before do
        new_placement
        get api_v1_order_url(new_order.id), headers: { Authorization: authorization }, as: :json
      end

      it 'returns order not found if order not exists', skip_before: true do
        get api_v1_order_url(new_order.id.to_i + 1), headers: { Authorization: authorization }, as: :json
        expect(response).to have_http_status(:not_found)
      end

      it 'show order' do
        expect(response).to have_http_status(:success)
      end

      it 'show order response contains attributes' do
        expect(json_response.dig(:included, 0, :attributes, :title)).to match(Order.last.products.first.title)
      end
    end
  end

  describe 'if user wants to create an order' do
    context 'when user is not logged in' do
      it 'does not create' do
        post api_v1_orders_url, params: new_order, as: :json
        expect(response).to have_http_status(:forbidden)
      end
    end

    context 'when user is logged in' do
      it 'does creates it' do
        post api_v1_orders_url, headers: { Authorization: authorization },
                                params: { order: { product_ids_and_quantities: [
                                  { product_id: new_product.id, quantity: 1 }
                                ] } }, as: :json
        expect(response).to have_http_status(:created)
      end
    end
  end

  describe 'when user create 1 order with product quantity' do
    context 'when user is not logged in' do
      it 'does not create' do
        post api_v1_orders_url, params: new_order, as: :json
        expect(response).to have_http_status(:forbidden)
      end
    end

    # rubocop:disable RSpec/MultipleMemoizedHelpers
    context 'when user is logged in, it builds 2 placements' do
      let(:product1) { create(:product, quantity: 10) }
      let(:product2) { create(:product, quantity: 10) }

      before do
        post api_v1_orders_url, headers: { Authorization: authorization },
                                params: { order: { product_ids_and_quantities: [
                                  { product_id: product1.id, quantity: 2 }, { product_id: product2.id, quantity: 3 }
                                ] } }, as: :json
      end

      it { expect(response).to have_http_status(:created) }

    end
    # rubocop:enable RSpec/MultipleMemoizedHelpers
  end
end
