# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id         :bigint           not null, primary key
#  price      :decimal(, )
#  published  :boolean
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint           not null
#
# Indexes
#
#  index_products_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe 'api/v1/product', type: :request do
  describe 'when product exists' do
    let(:new_product) { create(:product) }
    let(:authorization) { JsonWebToken.encode(user_id: new_product.user_id) }
    let(:new_user) { create(:user) }

    it 'shows all products' do
      get api_v1_products_url, as: :json
      expect(response).to have_http_status(:success)
    end

    it 'shows the product' do
      get api_v1_product_url(new_product), as: :json
      expect(response).to have_http_status(:success)
    end

    it 'updates if own the product' do
      patch api_v1_product_url(new_product), params: { product: { title: 'cambio de titulo' } },
                                             headers: { Authorization: authorization },
                                             as: :json
      expect(response).to have_http_status(:success)
    end

    it 'does not update if not product owner' do
      patch api_v1_product_url(new_product), params: { product: { title: 'cambio de titulo' } },
                                             headers: { Authorization: JsonWebToken.encode(user_id: new_user.id) },
                                             as: :json
      expect(response).to have_http_status(:forbidden)
    end

    it 'owner destroys the product' do
      delete api_v1_product_url(new_product), headers: { Authorization: authorization }, as: :json
      expect(response).to have_http_status(:no_content)
    end

    it 'forbids destroys the product if not owner' do
      delete api_v1_product_url(new_product), headers: { Authorization: JsonWebToken.encode(user_id: new_user.id) },
                                              as: :json
      expect(response).to have_http_status(:forbidden)
    end

    describe 'products attributes' do
      before do
        get api_v1_product_url(new_product), as: :json
      end

      let(:json_response) { JSON.parse(response.body, symbolize_names: true) }

      it 'has a title' do
        expect(json_response.dig(:data, :attributes, :title)).to match(new_product.title)
      end

      it 'has the user id' do
        expect(json_response.dig(:data, :relationships, :user, :data, :id).to_i).to eq(new_product.user_id)
      end

      it 'has the user email' do
        expect(json_response.dig(:included, 0, :attributes, :email)).to match(new_product.user.email)
      end
    end
  end

  describe 'when product does not exists' do
    let(:new_user) { create(:user) }
    let(:new_product) { build(:product) }
    let(:authorization) { JsonWebToken.encode(user_id: new_user.id) }

    it 'create a product' do
      post api_v1_products_url, params: {
        product: { title: 'producto de prueba', price: 242.51, published: true, user_id: new_user.id }
      }, headers: { Authorization: authorization }, as: :json
      expect(response).to have_http_status(:created)
    end

    it 'does not create a product' do
      post api_v1_products_url, params: { product: new_product }, as: :json
      expect(response).to have_http_status(:forbidden)
    end
  end

  describe 'when paginate all the products' do
    let(:new_product) { create_list(:product, 20) }
    let(:json_response) { JSON.parse(response.body, symbolize_names: true) }

    before do
      get api_v1_products_url, as: :json
    end

    it { expect(json_response.dig(:links, :first)).not_to be_nil }
    it { expect(json_response.dig(:links, :last)).not_to be_nil }
    it { expect(json_response.dig(:links, :prev)).not_to be_nil }
    it { expect(json_response.dig(:links, :next)).not_to be_nil }
  end
end
