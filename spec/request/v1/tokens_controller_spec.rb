# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'api/v1/tokens', type: :request do
  let(:new_user) { create(:user) }

  it 'creates and gets JWT token', skip_before: true do
    new_user2 = User.create(email: 'example@mail.com', password_digest: BCrypt::Password.create('g00d_pa$$'))

    post api_v1_tokens_url, params: { user: { email: new_user2.email, password: 'g00d_pa$$' } }, as: :json
    expect(response).to have_http_status(:success)
  end

  it 'expect json response not to be nil' do
    new_user2 = User.create(email: 'example@mail.com', password_digest: BCrypt::Password.create('g00d_pa$$'))
    post api_v1_tokens_url, params: { user: { email: new_user2.email, password: 'g00d_pa$$' } }, as: :json
    json_response = JSON.parse(response.body)
    expect(json_response).not_to be_nil
  end

  it 'does not get JWT token', skip_before: true do
    new_user2 = User.create(email: 'example@mail.com', password_digest: BCrypt::Password.create('g00d_pa$$'))

    post api_v1_tokens_url, params: { user: { email: new_user2.email, password: 'b@d_pa$$' } }, as: :json
    expect(response).to have_http_status(:unauthorized)
  end
end
