# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'api/v1/users', type: :request do
  let(:new_user) { create(:user) }
  let(:authorization) { JsonWebToken.encode(user_id: new_user.id) }
  let(:new_product) { create_list(:product, 5, user_id: new_user.id) }

  context 'when show user' do
    it 'shows user' do
      get api_v1_user_url(new_user), as: :json
      expect(response.status).to eq(200)
    end

    context 'when shows user data' do
      before do
        new_product
        get api_v1_user_url(new_user), as: :json
      end

      let(:json_response) { JSON.parse(response.body, symbolize_names: true) }

      it 'shows users email' do
        # test to ensure response contains the correct email
        expect(json_response.dig(:data, :attributes, :email)).to match(new_user.email)
      end

      it 'shows products related with user' do
        expect(json_response.dig(:data, :relationships, :products, :data, 0,
                                 :id).to_i).to eq(new_user.products.first.id)
      end

      it 'shows products title' do
        expect(json_response.dig(:included, 0, :attributes, :title)).to match(new_user.products.first.title)
      end
    end
  end

  context 'when create user with params are ok' do
    it 'creates user' do
      post api_v1_users_url, params: {
        user: { email: 'prueba@prueba.com', password: '123456' }
      }, as: :json
      expect(response).to have_http_status(:created)
    end
  end

  context 'when email is already on use' do
    it 'does not create user if email already exists' do
      post api_v1_users_url, params: {
        user: { email: new_user.email, password: '123456' }
      }, as: :json
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  context 'when user own the account' do
    it 'updates user' do
      patch api_v1_user_url(new_user),
            params: { user: { email: new_user.email, password: '123456' } },
            headers: { Authorization: authorization }, as: :json
      expect(response).to have_http_status(:success)
    end

    it 'destroys user' do
      # delete api_v1_user_url(new_user), as: :json
      # expect(response).to have_http_status(:no_content)
      delete api_v1_user_url(new_user),
             headers: { Authorization: authorization }, as: :json
      expect(response).to have_http_status(:no_content)
    end
  end

  context 'when user does not own the account' do
    it 'forbid update user' do
      patch api_v1_user_url(new_user),
            params: { user: { email: new_user.email, password: 'password' } }, as: :json
      expect(response).to have_http_status(:forbidden)
    end

    it 'forbid destroy user' do
      delete api_v1_user_url(new_user), as: :json
      expect(response).to have_http_status(:forbidden)
    end
  end
end
